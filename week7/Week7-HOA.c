#include <stdio.h>
#include <string.h>
#include <time.h>
#define ITEM_NAME_SIZE 20 /* max size of product name */
#define MAX_ITEMS       20 /* maximum number of products */

/*typedefs*/

typedef struct {
   int day,
       month,
       year;
} date_t;

typedef struct {
   char    type; //S: syrup, P: pills, I: injection
   date_t  packaging_date;
   date_t  expiration_date;
   int   aisle_number;
   char  aisle_side;
} medication_t;

typedef struct {
   char  type; //K: skin, H: hair
   int   aisle_number;
   char  aisle_side;
} skin_hair_t;

typedef struct {
   date_t  packaging_date;
   date_t expiration_date;
} dental_t;

typedef struct {
   date_t  packaging_date;
   date_t  expiration_date;
   int     aisle_number;
   char    aisle_side;
} vitamin_t;


typedef union {
   medication_t med;
   skin_hair_t sh;
   dental_t dent;
   vitamin_t vit;
} category_t;

typedef struct {
   char        item_name[ITEM_NAME_SIZE];
   int         unit_cost;
   char        product_category;
   category_t  category;
} pharmacy_item_t;

/* Prototypes */
void print_item(const pharmacy_item_t item);
int loadItems(pharmacy_item_t items[]);
int searchItem(const pharmacy_item_t items[], int size, char name[]);
int isExpired(const date_t d);

int main(void)
{
   pharmacy_item_t items[MAX_ITEMS];
   int i, n;

   n = loadItems(items);

   puts("--------PHARMACY PRODUCT LIST--------");
   for (i = 0;  i < n;  ++i)
      print_item(items[i]);


   char s1[] = "VitaminB";
   char s2[] = "CoughSyrup";
   printf("\n\nSEARCH STARTS FOR %s -- %s....",s1,s2);
   printf("\n%d %s items found.",searchItem(items,n,s1), s1);
   printf("\n%d %s items found.\n\n",searchItem(items,n,s2), s2);

   return (0);
}

int isExpired(const date_t d)
{
    //    time_t t = time(NULL);
//    struct tm *tm = localtime(&t);
//    char s[64];
//    strftime(s, sizeof(s), "%c", tm);
//    printf("\nToday is: %s\n", s);

   //Assuming it is March 23, 2018
   if(d.year < 2018) //expired
        return 1;
   else if(d.year > 2018) //not expired yet
   {
       return 0;
   }
   else //both 2018
   {
        if(d.month > 3) //not expired yet
            return 0;
        else if(d.month < 3) //expired
            return 1;
        else //both March 2018
        {
            if(d.day > 23)
                return 0; //not expired yet
            else
                return 1;
        }
   }
}

int searchItem(const pharmacy_item_t items[], int size, char name[])
{
    int count = 0;
    for(int i = 0; i < size; i++)
    {
        if(strcmp(items[i].item_name,name) == 0)
        {
            print_item(items[i]);
            count++;
        }
    }
    return count;
}

void print_item(const pharmacy_item_t item)
{
   printf("\n**%s**\n Unit cost:%dTL--", item.item_name, item.unit_cost);

   switch(item.product_category) {
   case 'M':
   case 'm':
       printf("Type:%c--Packaging:%d/%d/%d--Expiration:%d/%d/%d--"
              "Aisle:%d%c", item.category.med.type,
                     item.category.med.packaging_date.day,
                     item.category.med.packaging_date.month,
                     item.category.med.packaging_date.year,
                     item.category.med.expiration_date.day,
                     item.category.med.expiration_date.month,
                     item.category.med.expiration_date.year,
                     item.category.med.aisle_number,
                     item.category.med.aisle_side);
              break;

   case 'S':
   case 's':
        printf("Type:%c--Aisle:%d%c", item.category.sh.type,
                     item.category.sh.aisle_number,
                     item.category.sh.aisle_side);
              break;

   case 'D':
   case 'd':
        printf("Expiration:%d/%d/%d"
                ,item.category.dent.expiration_date.day,
                     item.category.dent.expiration_date.month,
                     item.category.dent.expiration_date.year);
              break;

   case 'V':
   case 'v':
        printf("Packaging:%d/%d/%d--Expiration:%d/%d/%d--"
              "Aisle:%d%c",
                     item.category.vit.packaging_date.day,
                     item.category.vit.packaging_date.month,
                     item.category.vit.packaging_date.year,
                     item.category.vit.expiration_date.day,
                     item.category.vit.expiration_date.month,
                     item.category.vit.expiration_date.year,
                     item.category.vit.aisle_number,
                     item.category.vit.aisle_side);
        break;
   }
}

int loadItems (pharmacy_item_t items[])
{
    char file_name[13];

    printf("Enter the name(max. 12 characters) of the file whose data should be read => ");
    scanf("%s", &file_name);

    FILE *infile;
    char line[80];
    int i = 0;
    int exp = 0;
    int status;

    infile = fopen(file_name, "r");

    while(infile == NULL) 	{
        printf("\nCannot open %s for input.\n\n", file_name);
        printf("Re-enter file name => ");
        scanf("%s", file_name);
        infile = fopen(file_name, "r");
    }

    while(i < MAX_ITEMS &&  status != EOF)
    {
        status = fscanf(infile, "%s", items[i].item_name);

            fscanf(infile, "%d", &items[i].unit_cost);
            fscanf(infile, " %c", &items[i].product_category);

            switch(items[i].product_category)
            {
              case 'M':
              case 'm':
                  fscanf(infile, " %c", &(items[i].category.med.type));
                  fscanf(infile, "%d %d %d", &items[i].category.med.packaging_date.day,
                        &items[i].category.med.packaging_date.month,
                        &items[i].category.med.packaging_date.year);
                  fscanf(infile, "%d %d %d", &items[i].category.med.expiration_date.day,
                        &items[i].category.med.expiration_date.month,
                        &items[i].category.med.expiration_date.year);
                  fscanf(infile, "%d %c", &items[i].category.med.aisle_number,
                        &items[i].category.med.aisle_side);
                  if(isExpired(items[i].category.med.expiration_date))
                   {
                        exp++;
                        i--;
                    }
                  break;
             case 'D':
             case 'd':
                 fscanf(infile, "%d %d %d", &items[i].category.dent.packaging_date.day,
                        &items[i].category.dent.packaging_date.month,
                        &items[i].category.dent.packaging_date.year);
                  fscanf(infile, "%d %d %d", &items[i].category.dent.expiration_date.day,
                        &items[i].category.dent.expiration_date.month,
                        &items[i].category.dent.expiration_date.year);
                  if(isExpired(items[i].category.dent.expiration_date))
                   {
                       exp++;
                       i--;
                   }
                  break;
             case 'S':
             case 's':
                  fscanf(infile, " %c", &(items[i].category.sh.type));
                  fscanf(infile, "%d %c", &items[i].category.sh.aisle_number,
                        &items[i].category.sh.aisle_side);
                  break;
              case 'V':
              case 'v':
                  fscanf(infile, "%d %d %d", &items[i].category.vit.packaging_date.day,
                        &items[i].category.vit.packaging_date.month,
                        &items[i].category.vit.packaging_date.year);
                  fscanf(infile, "%d %d %d", &items[i].category.vit.expiration_date.day,
                        &items[i].category.vit.expiration_date.month,
                        &items[i].category.vit.expiration_date.year);
                  fscanf(infile, "%d %c", &items[i].category.vit.aisle_number,
                        &items[i].category.vit.aisle_side);
                  break;
            }
        if(status != EOF)
            i++;
    }

    if(status != EOF && i == MAX_ITEMS)
        printf("Too much data in input file. Some data will be truncated.\n");

    printf("Loading Complete.\n%d expired items found.\n\n",exp);
    fclose(infile);  /* close file*/
    printf("File Closed.\n\n");
    return i;
}

