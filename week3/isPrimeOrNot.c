#include <stdio.h>

int isPrime(int number, int halfOfTheNumber){

    if(halfOfTheNumber == 1){
        return 1;
    }
    else
    {
       if(number % halfOfTheNumber == 0)
         return 0;
       else
         isPrime(number, halfOfTheNumber - 1);
    }
}

int main(){

    int number;
    printf("Enter a number: ");
    scanf("%d", &number);
    if (isPrime(number, number/2))
    {
        printf("%d is a prime number", number);
    }
    else
    {
        printf("%d is not a prime", number);
    }
    
    return 0;
}


