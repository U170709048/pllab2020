#include <stdio.h>

int gcd(int firstNumber, int secondNumber) {
    
    if (secondNumber != 0)
    {
        return gcd(secondNumber, firstNumber % secondNumber);
    }
    else
    {
        return firstNumber;
    }
}

int main() {
    int firstNumber, secondNumber;
    printf("Enter two numbers: ");
    scanf("%d %d", &firstNumber, &secondNumber);
    printf("The result of GCD is %d.", firstNumber, secondNumber, gcd(firstNumber, secondNumber));
    return 0;
}

